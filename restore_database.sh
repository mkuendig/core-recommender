#!/usr/bin/env bash

dropdb core_recommender
createdb core_recommender
psql core_recommender -f ./core_recommender_backup.sql