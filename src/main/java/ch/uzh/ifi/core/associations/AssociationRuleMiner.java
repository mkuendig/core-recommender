package ch.uzh.ifi.core.associations;

import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AlgoAgrawalFaster94;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AssocRules;
import ca.pfv.spmf.algorithms.frequentpatterns.fpgrowth.AlgoFPGrowth;
import ca.pfv.spmf.patterns.itemset_array_integers_with_count.Itemsets;
import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.rating.Rating;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.User;
import ch.uzh.ifi.core.data.user.UserDAO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class AssociationRuleMiner {

    private final double MIN_SUPPORT = 0.3;
    private final double MIN_CONFIDENCE = 0.5;
    private UserDAO userDAO;
    private RatingDAO ratingDAO;

    public AssociationRuleMiner() {
        DAOBuilder daoBuilder = DAOBuilder.getInstance();
        userDAO = daoBuilder.getUserDAO();
        ratingDAO = daoBuilder.getRatingDAO();
    }

    public AssocRules getAssocRules() {
        AssocRules rules = null;
        try {
            String inputFileName = "AssociationBaskets.txt";
            writeInputFile(inputFileName);
            // STEP 1: Applying the FP-GROWTH algorithm to find frequent itemsets
            AlgoFPGrowth fpgrowth = new AlgoFPGrowth();
            Itemsets patterns = fpgrowth.runAlgorithm(inputFileName, null, MIN_SUPPORT);
            int databaseSize = fpgrowth.getDatabaseSize();
            // STEP 2: Generating all rules from the set of frequent itemsets (based on Agrawal & Srikant, 94)
            AlgoAgrawalFaster94 algoAgrawal = new AlgoAgrawalFaster94();
            rules = algoAgrawal.runAlgorithm(patterns, null, databaseSize, MIN_CONFIDENCE);
            return rules;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rules;
    }

    private void writeInputFile(String inputFileName) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(inputFileName, "UTF-8");
        List<User> users = userDAO.findAllUsers();
        for (User user : users) {
            List<Rating> ratings = ratingDAO.findRatingsByUser(user);
            String line = "";
            for (Rating rating : ratings) {
                line += rating.getItemId() + " ";
            }
            if (line.length() > 1) {
                writer.println(line.substring(0, line.length() - 1));
            }
        }
        writer.close();
    }
}
