package ch.uzh.ifi.core.associations;

import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AssocRule;
import ca.pfv.spmf.algorithms.associationrules.agrawal94_association_rules.AssocRules;
import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.rating.Rating;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.User;
import ch.uzh.ifi.core.data.user.UserDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AssociationRulePredictor {

    private final RatingDAO ratingDAO;
    private final UserDAO userDAO;
    private final AssociationRuleMiner assocRuleMiner;
    private AssocRules assocRules;

    public AssociationRulePredictor() {
        DAOBuilder daoBuilder = DAOBuilder.getInstance();
        userDAO = daoBuilder.getUserDAO();
        ratingDAO = daoBuilder.getRatingDAO();
        assocRuleMiner = new AssociationRuleMiner();
        assocRules = assocRuleMiner.getAssocRules();
    }

    public List<Long> getModulePredictions(long userId) {

        List<Long> predictions = new ArrayList<>();
        User user = userDAO.findOneByMeteorId(userId);
        List<Rating> userRatings = ratingDAO.findRatingsByUser(user);
        ArrayList<Long> ratedModules = userRatings.stream().map((Function<Rating, Long>) Rating::getItemId).collect(Collectors.toCollection(ArrayList::new));

        for (AssocRule assocRule : assocRules.getRules()) {
            //check if user has rated all modules in the itemset and not rated all modules in the resultset
            ArrayList<Long> itemSet1 = new ArrayList<>();
            for (int i : assocRule.getItemset1()) {
                itemSet1.add((long) i);
            }
            ArrayList<Long> itemSet2 = new ArrayList<>();
            for (int i : assocRule.getItemset2()) {
                itemSet2.add((long) i);
            }
            if (ratedModules.containsAll(itemSet1)) {
                if (!ratedModules.containsAll(itemSet2)) {
                    for (long item : itemSet2) {
                        if (!ratedModules.contains(item) && !predictions.contains(item)) {
                            predictions.add(item);
                        }
                    }
                }
            }
        }
        System.out.println(predictions);
        return predictions;
    }

    public void recalculateAssocRules() {
        assocRules = assocRuleMiner.getAssocRules();
    }
}
