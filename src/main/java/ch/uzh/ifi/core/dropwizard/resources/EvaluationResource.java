package ch.uzh.ifi.core.dropwizard.resources;

import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.UserDAO;
import ch.uzh.ifi.core.dropwizard.views.SimilarModulesView;
import ch.uzh.ifi.core.recommender.Evaluation;
import ch.uzh.ifi.core.recommender.RecBuilder;
import ch.uzh.ifi.core.recommender.params.EvalParams;
import ch.uzh.ifi.core.recommender.params.RecParams;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.eval.TaskExecutionException;
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity;
import org.grouplens.lenskit.vectors.similarity.PearsonCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

@Path("/evaluate")
public class EvaluationResource {

    private static final Logger logger = LoggerFactory.getLogger(EvaluationResource.class);
    private final ModuleDAO moduleDAO;
    private final UserDAO userDAO;
    private final RatingDAO ratingDAO;

    public EvaluationResource() {
        moduleDAO = DAOBuilder.getInstance().getModuleDAO();
        userDAO = DAOBuilder.getInstance().getUserDAO();
        ratingDAO = DAOBuilder.getInstance().getRatingDAO();
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/cb")
    public List<Evaluation.EvaluationResult> cbCrossValidation(
            @QueryParam("studyCourse") String studyCourse,
            @QueryParam("similarModulesCount") int similarModulesCount,
            @QueryParam("similarModuleDecrease") double similarModuleDecrease,
            @QueryParam("vectorSimilarity") String vectorSimilarity,
            @QueryParam("ratingCount") int ratingCount)
            throws TaskExecutionException, IOException {

        long timestamp = 0;
        if (ratingCount > 0) {
            timestamp = ratingDAO.getTimestampOfRatingCount(ratingCount);
        }

        if (similarModulesCount > 0)
            EvalParams.SIMILAR_MODULES_COUNT = similarModulesCount;
        if (similarModuleDecrease > 0)
            EvalParams.SIMILAR_MODULE_DECREASE = similarModuleDecrease;
        if (vectorSimilarity != null && vectorSimilarity.equals("cosine")) {
            EvalParams.VECTOR_SIMILARITY = CosineVectorSimilarity.class;
        } else if (vectorSimilarity != null && vectorSimilarity.equals("pearson")) {
            EvalParams.VECTOR_SIMILARITY = PearsonCorrelation.class;
        }
        Evaluation evaluation = new Evaluation(studyCourse, timestamp);
        return evaluation.getCBEvaluationResultList();
    }


    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/cf")
    public List<Evaluation.EvaluationResult> cfCrossValidation(
            @QueryParam("method") String method,
            @QueryParam("studyCourse") String studyCourse,
            @QueryParam("vectorSimilarity") String vectorSimilarity,
            @QueryParam("neighbourhoodSize") int neighbourhoodSize,
            @QueryParam("minNeighborCount") int minNeighborCount,
            @QueryParam("ratingCount") int ratingCount)
            throws TaskExecutionException, IOException {

        long timestamp = 0;
        if (ratingCount > 0) {
            timestamp = ratingDAO.getTimestampOfRatingCount(ratingCount);
        }

        if (vectorSimilarity != null && vectorSimilarity.equals("cosine")) {
            EvalParams.VECTOR_SIMILARITY = CosineVectorSimilarity.class;
        } else if (vectorSimilarity != null && vectorSimilarity.equals("pearson")) {
            EvalParams.VECTOR_SIMILARITY = PearsonCorrelation.class;
        }

        if (neighbourhoodSize > 0) {
            EvalParams.NEIGHBORHOOD_SIZE = neighbourhoodSize;
        }

        if (minNeighborCount > 0) {
            EvalParams.MIN_NEIGHBOR_COUNT = minNeighborCount;
        }

        Evaluation evaluation = new Evaluation(studyCourse, timestamp);

        if (method != null && method.equals("item-item")) {
            return evaluation.getItemItemEvaluationResultList();
        } else {
            return evaluation.getUserUserEvaluationResultList();
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/hybrid")
    public List<Evaluation.EvaluationResult> hybridCrossValidation(
            @QueryParam("studyCourse") String studyCourse,
            @QueryParam("vectorSimilarity") String vectorSimilarity,
            @QueryParam("cfPercentage") double cfPercentage)
            throws TaskExecutionException, IOException {

        if (vectorSimilarity != null && vectorSimilarity.equals("cosine")) {
            EvalParams.VECTOR_SIMILARITY = CosineVectorSimilarity.class;
        } else if (vectorSimilarity != null && vectorSimilarity.equals("pearson")) {
            EvalParams.VECTOR_SIMILARITY = PearsonCorrelation.class;
        }

        if (cfPercentage > 0.001) {
            EvalParams.HYBRID_CF_PERCENTAGE = cfPercentage;
        }

        Evaluation evaluation = new Evaluation(studyCourse);
        return evaluation.getHybridEvaluationResultList();
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/average")
    public List<Evaluation.EvaluationResult> averageCrossValidation() throws TaskExecutionException, IOException {
        Evaluation evaluation = new Evaluation();
        return evaluation.getUserMeanEvaluationResultList();
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/constant")
    public List<Evaluation.EvaluationResult> constantCrossValidation(@QueryParam("constant") double constant) throws TaskExecutionException, IOException {
        Evaluation evaluation = new Evaluation();
        return evaluation.getUserConstantEvaluationResultList(constant);
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cross-validation/svd")
    public List<Evaluation.EvaluationResult> funkSVDCrossValidation() throws TaskExecutionException, IOException {
        Evaluation evaluation = new Evaluation();
        return evaluation.getFunkSVDEvaluationResultList();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/gini")
    public String giniCoefficient() throws TaskExecutionException, IOException, RecommenderBuildException {
        List<Long> userIds = userDAO.findAllUserIds();
        List<Long> moduleIds = moduleDAO.findAllModuleIds();
        ItemScorer cbScorer = RecBuilder.buildCBRecommender().getItemScorer();
        ItemScorer cfScorer = RecBuilder.buildCFRecommender().getItemScorer();
        DoubleSummaryStatistics averageMeans = new DoubleSummaryStatistics();
        DoubleSummaryStatistics averageGinis = new DoubleSummaryStatistics();

        for (Long userId : userIds) {
            ArrayList<Double> diffValues = new ArrayList<>();
            DoubleSummaryStatistics cfStats = new DoubleSummaryStatistics();
            DoubleSummaryStatistics cbStats = new DoubleSummaryStatistics();
            DoubleSummaryStatistics differenceStats = new DoubleSummaryStatistics();
            for (Long moduleId : moduleIds) {
                double cfScore = cfScorer.score(userId, moduleId);
                double cbScore = cbScorer.score(userId, moduleId);
                if (!Double.isNaN(cfScore) && !Double.isNaN(cbScore)) {
                    cfStats.accept(cfScore);
                    cbStats.accept(cbScore);
                    double diff = Math.abs(cfScore - cbScore);
                    logger.info("Difference:: {}", diff);
//                    if (diff < 0.25) {
//                        Module module = moduleDAO.findOneById(moduleId);
//                        double avgRating = ratingDAO.getAverageRating(module);
//                        long numberOfRatings = ratingDAO.getRatingsCount(module);
//                        DecimalFormat df = new DecimalFormat("0.0");
//                        logger.info("\"{}\";\"{}\";\"{}\";\"{}\";\"{}\";\"{}\";\"{}\"",
//                                df.format(cfScore), df.format(cbScore), df.format(diff), df.format(avgRating),
//                                numberOfRatings, module.getMeteorModuleId(), module.getModuleTitle());
//                    }
                    differenceStats.accept(diff);
                    diffValues.add(diff);
                }
            }
            double diffMean = differenceStats.getAverage();
            double relVars = 0;
            for (Double i : diffValues) {
                for (Double j : diffValues) {
                    if (!i.equals(j))
                        relVars += Math.abs(i - j);
                }
            }
            relVars = relVars / (2.0 * diffValues.size() * diffValues.size());
            double giniCoefficient = relVars / diffMean;
            logger.info("Avg CF Score: {} - Avg CB Score: {}", cfStats.getAverage(), cbStats.getAverage());
            logger.info("Avg difference cb/cf for user {}: {}", userId, diffMean);
            logger.info("Number of modules: {}", diffValues.size());
            logger.info("Gini coefficient: {}", giniCoefficient);
            averageGinis.accept(giniCoefficient);
            averageMeans.accept(diffMean);
        }
        logger.info("Average Gini coefficient: {}", averageGinis.getAverage());
        logger.info("Average mean: {}", averageMeans.getAverage());
        return "Gini = " + averageGinis.getAverage() + "\nAverage mean = " + averageMeans.getAverage();
    }


    @GET
    @Timed
    @UnitOfWork
    @Path("/similar-modules")
    public SimilarModulesView similarModules(@QueryParam("moduleId") String meteorModuleId) throws RecommenderBuildException {
        Module module = moduleDAO.findOneByMeteorId(meteorModuleId);
        Long moduleId = module.getId();
        Evaluation evaluation = new Evaluation();
        String moduleTitle = module.getModuleTitle();
        SimilarModulesView similarModulesView = new SimilarModulesView(meteorModuleId, moduleTitle);
        SortedMap<Double, Long> resultMap = evaluation.findSimilarModules(moduleId);
        int numberOfRecords = 20;
        for (Map.Entry<Double, Long> resultEntry : resultMap.entrySet()) {
            Double similarity = resultEntry.getKey();
            module = moduleDAO.findOneById(resultEntry.getValue());
            similarModulesView.addSimilarModule(module.getMeteorModuleId(), module.getModuleTitle(), similarity);
            numberOfRecords--;
            if (numberOfRecords == 0) {
                break;
            }
        }
        return similarModulesView;
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/facts")
    public String stats() {
        long numberOfModules = moduleDAO.getModulesCount();
        long numberOfRatings = ratingDAO.getRatingsCount();
        long numberOfUsers = userDAO.getUsersCount();
        long numberOfWeightedFeatures = moduleDAO.getWeightedFeaturesCount();

        return "Number of Modules: " + numberOfModules + "\n" +
                "Number of Ratings: " + numberOfRatings + "\n" +
                "Number of Users: " + numberOfUsers + "\n" +
                "Number of weighted features: " + numberOfWeightedFeatures;
    }
}
