package ch.uzh.ifi.core.dropwizard.resources;

import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.User;
import ch.uzh.ifi.core.data.user.UserDAO;
import ch.uzh.ifi.core.recommender.RecBuilder;
import ch.uzh.ifi.core.recommender.UserNotFoundException;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.hibernate.UnitOfWork;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.scored.ScoredId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.*;


@Path("/predictions")
@Produces(MediaType.APPLICATION_JSON)
public class PredictionResource {

    private static final Logger logger = LoggerFactory.getLogger(PredictionResource.class);
    private RatingDAO ratingDAO = DAOBuilder.getInstance().getRatingDAO();
    private UserDAO userDAO = DAOBuilder.getInstance().getUserDAO();
    private ModuleDAO moduleDAO = DAOBuilder.getInstance().getModuleDAO();
    private ItemRecommender primaryRecommender;
    private long ratingCountInHybridModel = -1;

    /**
     * @param meteorUserId: _id of user in meteor
     * @throws RecommenderBuildException
     * @return: json of predictions
     */
    @GET
    @UnitOfWork
    @Timed
    @Path("/primary")
    public List<Prediction> getHybridPredictions(@QueryParam("meteorUserId") String meteorUserId) throws RecommenderBuildException {
        User user = userDAO.findOneByMeteorId(meteorUserId);
        int recomputeThreshold = 10;
        if (ratingDAO.getRatingsCount() - ratingCountInHybridModel > recomputeThreshold || primaryRecommender == null) {
            logger.info("hybrid recompute threshold exceeded - rebuilding model");
            buildPrimaryRecommender();
        }
        Set<Long> allModules = new HashSet<>(moduleDAO.findAllModuleIds());
        List<ScoredId> scoredIds;
        try {
            scoredIds = primaryRecommender.recommend(user.getId(), -1, allModules, null);
        } catch (UserNotFoundException u) {
            logger.info("user not found in hybrid model - rebuilding model");
            buildPrimaryRecommender();
            scoredIds = primaryRecommender.recommend(user.getId(), -1, allModules, null);
        }
        return predictionsFromScoredIds(meteorUserId, scoredIds);
    }

    private void buildPrimaryRecommender() throws RecommenderBuildException {
        primaryRecommender = RecBuilder.buildHybridRecommender().getItemRecommender();
        ratingCountInHybridModel = ratingDAO.getRatingsCount();
    }


    @GET
    @UnitOfWork
    @Timed
    @Path("/cb")
    public List<Prediction> getCFPredictions(@QueryParam("meteorUserId") String meteorUserId) throws InterruptedException, RecommenderBuildException {
        User user = userDAO.findOneByMeteorId(meteorUserId);
        ItemRecommender itemRecommender = RecBuilder.buildCBRecommender().getItemRecommender();
        Set<Long> allModules = new HashSet<>(moduleDAO.findAllModuleIds());
        List<ScoredId> cbScores = itemRecommender.recommend(user.getId(), -1, allModules, null);
        return predictionsFromScoredIds(meteorUserId, cbScores);
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/cf")
    public List<Prediction> getCBPredictions(@QueryParam("meteorUserId") String meteorUserId) throws InterruptedException, RecommenderBuildException {
        User user = userDAO.findOneByMeteorId(meteorUserId);
        ItemRecommender itemRecommender = RecBuilder.buildCFRecommender().getItemRecommender();
        List<ScoredId> cfScores = itemRecommender.recommend(user.getId());
        return predictionsFromScoredIds(meteorUserId, cfScores);
    }


    private ArrayList<Prediction> predictionsFromScoredIds(String meteorUserId, List<ScoredId> scoredIds) {
        ArrayList<Prediction> predictions = new ArrayList<>();
        if (scoredIds == null) return null;
        for (ScoredId scoredId : scoredIds) {
            long moduleId = scoredId.getId();
            double score = scoredId.getScore();
            if (Double.isNaN(score)) {
                continue;
            } else if (score < 0) {
                score = 0.0;
            } else if (score > 5.0) {
                score = 5.0;
            }
            Module module = moduleDAO.findOneById(moduleId);
            if (module != null) {
                predictions.add(new Prediction(meteorUserId, module.getMeteorModuleId(), score));
            } else {
                logger.error("Prediction Error: module not found - id {}", moduleId);
            }
        }
        return predictions;
    }

    public class Prediction { // JSON notation of prediction
        private String meteorUserId;
        private String meteorModuleId;
        private double rating;

        public Prediction(String meteorUserId, String meteorModuleId, double rating) {
            this.meteorUserId = meteorUserId;
            this.meteorModuleId = meteorModuleId;
            this.rating = rating;
        }

        @JsonProperty
        public String getMeteorUserId() {
            return meteorUserId;
        }

        @JsonProperty
        public String getMeteorModuleId() {
            return meteorModuleId;
        }

        @JsonProperty
        public double getRating() {
            return rating;
        }
    }
}