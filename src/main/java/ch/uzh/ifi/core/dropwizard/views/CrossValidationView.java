package ch.uzh.ifi.core.dropwizard.views;

import io.dropwizard.views.View;
import org.grouplens.lenskit.util.table.Row;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CrossValidationView extends View {

    private Map<String, AlgorithmTemplate> algorithmTemplates;

    public CrossValidationView() {
        super("/views/cross-validation.mustache");
        this.algorithmTemplates = new HashMap<String, AlgorithmTemplate>();
    }

    Collection<AlgorithmTemplate> rows() {
        return algorithmTemplates.values();
    }

    public void addRowToAlgorithm(String algorithm, Row partitionRow) {
        AlgorithmTemplate algorithmTemplate = algorithmTemplates.get(algorithm);
        if (algorithmTemplate == null) {
            algorithmTemplate = new AlgorithmTemplate(algorithm);
            algorithmTemplates.put(algorithm, algorithmTemplate);
        }
        algorithmTemplate.addPartition(partitionRow);
    }

    private class AlgorithmTemplate {

        private String algorithm;
        private int partitions;
        private double rmseByRatingSum = 0;

        public AlgorithmTemplate(String algorithm) {
            this.algorithm = algorithm;
            partitions = 0;
        }

        public void addPartition(Row partitionRow) {
            Object rmseByRating = partitionRow.value("RMSE.ByRating");
            if (rmseByRating != null) {
                rmseByRatingSum += (Double) rmseByRating;
                partitions++;
            }
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public Double getRMSEByRating() {
            return rmseByRatingSum / partitions;
        }

        public int getPartitions() {
            return partitions;
        }

    }

}