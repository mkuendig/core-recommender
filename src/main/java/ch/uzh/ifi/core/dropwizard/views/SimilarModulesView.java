package ch.uzh.ifi.core.dropwizard.views;

import io.dropwizard.views.View;

import java.util.ArrayList;
import java.util.Collection;

public class SimilarModulesView extends View {

    String meteorModuleId;
    String moduleTitle;
    private Collection<SimilarModuleTemplate> similarModules;

    public SimilarModulesView(String meteorModuleId, String moduleTitle) {
        super("/views/similar-modules.mustache");
        similarModules = new ArrayList<SimilarModuleTemplate>();
        this.meteorModuleId = meteorModuleId;
        this.moduleTitle = moduleTitle;
    }

    public void addSimilarModule(String meteorModuleId, String moduleTitle, double similarity) {
        SimilarModuleTemplate similarModuleTemplate = new SimilarModuleTemplate(meteorModuleId, moduleTitle, similarity);
        similarModules.add(similarModuleTemplate);
    }

    protected Collection<SimilarModuleTemplate> getSimilarModules() {
        return similarModules;
    }

    public String getMeteorModuleId() {
        return meteorModuleId;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    private class SimilarModuleTemplate {
        String meteorModuleId;
        String moduleTitle;
        Double similarity;

        public SimilarModuleTemplate(String meteorModuleId, String moduleTitle, Double similarity) {
            this.meteorModuleId = meteorModuleId;
            this.moduleTitle = moduleTitle;
            this.similarity = similarity;
        }

        public String getMeteorModuleId() {
            return meteorModuleId;
        }

        public void setMeteorModuleId(String meteorModuleId) {
            this.meteorModuleId = meteorModuleId;
        }

        public String getModuleTitle() {
            return moduleTitle;
        }

        public void setModuleTitle(String moduleTitle) {
            this.moduleTitle = moduleTitle;
        }

        public Double getSimilarity() {
            return similarity;
        }

        public void setSimilarity(Double similarity) {
            this.similarity = similarity;
        }
    }
}