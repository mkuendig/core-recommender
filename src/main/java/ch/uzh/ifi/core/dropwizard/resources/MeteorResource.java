package ch.uzh.ifi.core.dropwizard.resources;

import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.User;
import ch.uzh.ifi.core.data.user.UserDAO;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/meteor")
@Produces(MediaType.APPLICATION_JSON)
public class MeteorResource {

    private static final Logger logger = LoggerFactory.getLogger(MeteorResource.class);
    private final RatingDAO ratingDAO;
    private final UserDAO userDAO;
    private final ModuleDAO moduleDAO;

    public MeteorResource() {
        DAOBuilder daoBuilder = DAOBuilder.getInstance();
        this.ratingDAO = daoBuilder.getRatingDAO();
        this.userDAO = daoBuilder.getUserDAO();
        this.moduleDAO = daoBuilder.getModuleDAO();
    }

    @Path("/ratings")
    @POST
    @UnitOfWork
    @Timed
    public String importRatings(@Valid MeteorRating[] meteorRatings) {
        for (MeteorRating meteorRating : meteorRatings) {
            String meteorModuleId = meteorRating.getMeteorModuleId();
            Module module = moduleDAO.findOneByMeteorId(meteorModuleId);
            if (module == null) {
                module = moduleDAO.create(meteorModuleId);
            }
            User user = userDAO.findOrCreate(meteorRating.getMeteorUserId());
            user.setStudyCourse(meteorRating.getStudyCourse());
            ratingDAO.createOrUpdate(module, user, meteorRating.getRating(), meteorRating.getCreatedAt());
            logger.info("updated rating for module {} - user {}", module.getMeteorModuleId(), user.getMeteorUserId());
        }
        return "Updated " + meteorRatings.length + " ratings";
    }

    @Path("/ratings")
    @DELETE
    @UnitOfWork
    @Timed
    public String deleteRating(MeteorRating meteorRating) {
        Module module = moduleDAO.findOneByMeteorId(meteorRating.getMeteorModuleId());
        if (module == null) {
            String moduleNotFound = "Error deleting rating: module " + meteorRating.getMeteorModuleId() + " not found";
            logger.error(moduleNotFound);
            return moduleNotFound;
        }
        User user = userDAO.findOneByMeteorId(meteorRating.getMeteorUserId());
        if (user == null) {
            String userNotFound = "Error deleting rating: user " + meteorRating.getMeteorUserId() + " not found";
            logger.error(userNotFound);
            return userNotFound;
        }
        ratingDAO.deleteRating(user, module);
        return "Deleted rating for module " + module.getMeteorModuleId() + " for user " + user.getMeteorUserId();
    }

    @Path("/not-interested")
    @POST
    @UnitOfWork
    @Timed
    public String importNotInterested(@Valid NotInterestedModule[] notInterestedModules) {
        for (NotInterestedModule notInterestedModule : notInterestedModules) {
            User user = userDAO.findOrCreate(notInterestedModule.getMeteorUserId());
            Module module = moduleDAO.findOneByMeteorId(notInterestedModule.getMeteorModuleId());
            user.addNotInterestedModule(module);
            userDAO.saveUser(user);
        }
        return "Updated " + notInterestedModules.length + " not interested modules";
    }

    public static class NotInterestedModule {
        private String meteorUserId;
        private String meteorModuleId;

        public String getMeteorUserId() {
            return meteorUserId;
        }

        public void setMeteorUserId(String meteorUserId) {
            this.meteorUserId = meteorUserId;
        }

        public String getMeteorModuleId() {
            return meteorModuleId;
        }

        public void setMeteorModuleId(String meteorModuleId) {
            this.meteorModuleId = meteorModuleId;
        }
    }

    public static class MeteorRating {
        private String meteorUserId;
        private String meteorModuleId;
        private String studyCourse;
        private double rating;
        private long createdAt;

        public String getStudyCourse() {
            return studyCourse;
        }

        public void setStudyCourse(String studyCourse) {
            this.studyCourse = studyCourse;
        }

        public long getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
        }

        public String getMeteorUserId() {
            return meteorUserId;
        }

        public void setMeteorUserId(String meteorUserId) {
            this.meteorUserId = meteorUserId;
        }

        public String getMeteorModuleId() {
            return meteorModuleId;
        }

        public void setMeteorModuleId(String meteorModuleId) {
            this.meteorModuleId = meteorModuleId;
        }

        public double getRating() {
            return rating;
        }

        public void setRating(double rating) {
            this.rating = rating;
        }
    }
}
