package ch.uzh.ifi.core.dropwizard.resources;


import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@Path("/export/coursemgmt")
@Produces(MediaType.APPLICATION_JSON)
public class DataImportResource {

    private static final Logger logger = LoggerFactory.getLogger(DataImportResource.class);
    private final ModuleDAO moduleDAO;

    // Weighted Features
    boolean INCLUDE_PERSON_IN_CHARGE = false;


    public DataImportResource() {
        this.moduleDAO = DAOBuilder.getInstance().getModuleDAO();
    }

    public void extractCategorySlugs(Set<String> categorySlugs, ModuleCategory[] categories) {
        for (ModuleCategory category : categories) {
            for (ModuleCategoryField field : category.getFields()) {
                categorySlugs.add(field.getSlug());
            }
            extractCategorySlugs(categorySlugs, category.getParents());
        }
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/modules")
    public void importModules(ModuleImport[] moduleImports) {
        moduleDAO.clearWeightedFeatures();
        for (ModuleImport moduleImport : moduleImports) {
            String meteorModuleId = moduleImport.getModuleId();
            String moduleTitle = moduleImport.getTitle();
            String personInCharge = moduleImport.getPersonInChargeName();
            Module module = moduleDAO.findOneByMeteorId(meteorModuleId);
            if (module == null) {
                module = moduleDAO.create(meteorModuleId);
                logger.info("created new module {}", module.getMeteorModuleId());
            }
            module.setModuleTitle(moduleTitle);
            if (INCLUDE_PERSON_IN_CHARGE && personInCharge.length() > 0) {
                moduleDAO.createWeightedFeature(module, personInCharge, .5);
            }
            moduleDAO.persistModule(module);
            logger.info("updated features for module {}", module.getMeteorModuleId());
        }
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/features")
    public void importFeatures(ModuleFeature[] moduleFeatures) {
        Map<String, Module> modulesMap = moduleDAO.getModulesMap();
        for (ModuleFeature moduleFeature : moduleFeatures) {
            Module module = modulesMap.get(moduleFeature.getModuleId());
            moduleDAO.createWeightedFeature(module, moduleFeature.getFeatureName(), moduleFeature.getWeight());
            logger.info("added weighted feature to", module.getMeteorModuleId());
        }
        logger.info("cleared and recreated {} module features", moduleFeatures.length);
        moduleDAO.saveModules(modulesMap.values());
    }

    private static class ModuleImport {
        private String moduleId;
        private String title;
        private ModuleCategory[] categories;
        private ModulePersonInCharge[] personInCharge;
        private String language;
        private double ects;

        public String getModuleId() {
            return moduleId;
        }

        public void setModuleId(String moduleId) {
            this.moduleId = moduleId;
        }


        public ModulePersonInCharge[] getPersonInCharge() {
            return personInCharge;
        }

        public String getPersonInChargeName() {
            if (personInCharge.length != 0)
                return personInCharge[0].getFirstname() + " " + personInCharge[0].getLastname();
            else
                return "";
        }


        public void setPersonInCharge(ModulePersonInCharge[] personInCharge) {
            this.personInCharge = personInCharge;
        }

        public ModuleCategory[] getCategories() {
            return categories;
        }

        public void setCategories(ModuleCategory[] categories) {
            this.categories = categories;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public double getEcts() {
            return ects;
        }

        public void setEcts(double ects) {
            this.ects = ects;
        }
    }

    private static class ModuleCategory {
        ModuleCategoryField[] fields;
        ModuleCategory[] parents;

        public ModuleCategoryField[] getFields() {
            return fields;
        }

        public void setFields(ModuleCategoryField[] fields) {
            this.fields = fields;
        }

        public ModuleCategory[] getParents() {
            return parents;
        }

        public void setParents(ModuleCategory[] parents) {
            this.parents = parents;
        }
    }

    private static class ModuleCategoryField {
        String title;
        String slug;
        String module_type;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getModule_type() {
            return module_type;
        }

        public void setModule_type(String module_type) {
            this.module_type = module_type;
        }
    }

    private static class ModulePersonInCharge {
        String uzhId;
        String firstname;
        String lastname;
        String title;
        String position;

        public String getUzhId() {
            return uzhId;
        }

        public void setUzhId(String uzhId) {
            this.uzhId = uzhId;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }

    private static class ModuleFeature {
        private String moduleId;
        private String featureName;
        private double weight;

        public String getModuleId() {
            return moduleId;
        }

        public void setModuleId(String moduleId) {
            this.moduleId = moduleId;
        }

        public String getFeatureName() {
            return featureName;
        }

        public void setFeatureName(String featureName) {
            this.featureName = featureName;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }
    }
}