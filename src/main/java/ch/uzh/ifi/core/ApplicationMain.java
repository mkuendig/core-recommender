package ch.uzh.ifi.core;

import ch.uzh.ifi.core.conf.ApplicationConfiguration;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.WeightedFeature;
import ch.uzh.ifi.core.data.rating.Rating;
import ch.uzh.ifi.core.data.user.User;
import ch.uzh.ifi.core.dropwizard.resources.*;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.hibernate.SessionFactory;

public class ApplicationMain extends Application<ApplicationConfiguration> {

    private static SessionFactory sessionFactory;

    private final HibernateBundle<ApplicationConfiguration> hibernateBundle =
            new HibernateBundle<ApplicationConfiguration>(Rating.class, Module.class, User.class, WeightedFeature.class) {
                public DataSourceFactory getDataSourceFactory(ApplicationConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    public static void main(String[] args) throws Exception {
        new ApplicationMain().run(args);
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<ApplicationConfiguration>() {
            public DataSourceFactory getDataSourceFactory(ApplicationConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(new ViewBundle<ApplicationConfiguration>());
    }

    @Override
    public void run(ApplicationConfiguration appConfig, Environment environment) throws Exception {
        hibernateBundle.run(appConfig, environment);
        sessionFactory = hibernateBundle.getSessionFactory();

        environment.jersey().register(new PredictionResource());
        environment.jersey().register(new EvaluationResource());
        environment.jersey().register(new MeteorResource());
        environment.jersey().register(new DataImportResource());
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
