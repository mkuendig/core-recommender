package ch.uzh.ifi.core.data;

import org.grouplens.lenskit.data.dao.SortOrder;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomSQLStatementFactory extends BasicSQLStatementFactory {

    private static final Logger logger =
            LoggerFactory.getLogger(CustomSQLStatementFactory.class);

    private String ratingsTableName = "ratings";
    private String usersTableName = "users";
    private String userColumn = "user_id";
    private String moduleColumn = "module_id";
    private String ratingColumn = "rating";
    private String timestampColumn = "timestamp";
    private String studyCourseColumn = "study_course";
    private long minTimestamp = -1;
    private long maxTimestamp = -1;
    private String studyCourse;
    private long userId;

    public CustomSQLStatementFactory() {
        super.setTableName(ratingsTableName);
        super.setItemColumn(moduleColumn);
        super.setUserColumn(userColumn);
        super.setRatingColumn(ratingColumn);
        super.setTimestampColumn(timestampColumn);
    }

    public void setStudyCourse(String studyCourse) {
        this.studyCourse = studyCourse;
    }

    public void setMaxTimestamp(long maxTimestamp) {
        this.maxTimestamp = maxTimestamp;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    private void addTimestampClause(StringBuilder query) {
        if (minTimestamp > 0) {
            query.append(" AND ").append(timestampColumn).append(" > ").append(minTimestamp);
        }
        if (maxTimestamp > 0) {
            query.append(" AND ").append(timestampColumn).append(" < ").append(maxTimestamp);
        }
    }

    private void addStudyCourseClause(StringBuilder query) {
        if (studyCourse != null) {
            query.append(" AND ");
            query.append(usersTableName)
                    .append(".")
                    .append(studyCourseColumn).append(" = ")
                    .append("'" + studyCourse + "'");
        }
    }

    private void addUserClause(StringBuilder query) {
        if (userId > 0) {
            query.append(" OR ").append(userColumn).append(" = ").append(userId);
        }
    }


    private void addCustomClauses(StringBuilder query) {
        addTimestampClause(query);
        addStudyCourseClause(query);
        addUserClause(query);
    }

    protected void rqAddSelectFrom(StringBuilder query) {
        query.append("SELECT ");
        query.append(userColumn).append(", ");
        query.append(moduleColumn).append(", ");
        query.append(ratingColumn).append(", ");
        query.append(timestampColumn).append(", ");
        query.append(studyCourseColumn);
        query.append(" FROM ");
        query.append(ratingsTableName).append(", ");
        query.append(usersTableName);
        query.append(" WHERE ");
        query.append(ratingsTableName).append(".").append(userColumn);
        query.append(" = ");
        query.append(usersTableName).append(".").append("id");
    }

    @Override
    public String prepareEvents(SortOrder order) {
        StringBuilder query = new StringBuilder();
        rqAddSelectFrom(query);
        addCustomClauses(query);
        rqAddOrder(query, order);
        rqFinish(query);
        logger.info("Rating query: {}", query);
        return query.toString();
    }


    @Override
    public String prepareItemEvents() {
        StringBuilder query = new StringBuilder();
        rqAddSelectFrom(query);
        query.append(" AND ").append(moduleColumn).append(" = ?");
        addCustomClauses(query);
        rqFinish(query);
        logger.info("Item rating query: {}", query);
        return query.toString();
    }

    @Override
    public String prepareUserEvents() {
        StringBuilder query = new StringBuilder();
        rqAddSelectFrom(query);
        query.append(" AND ").append(userColumn).append(" = ?");
        addCustomClauses(query);
        rqFinish(query);
        logger.info("User rating query: {}", query);
        return query.toString();
    }
}
