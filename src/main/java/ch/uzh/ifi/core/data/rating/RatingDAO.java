package ch.uzh.ifi.core.data.rating;

import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.user.User;
import io.dropwizard.hibernate.AbstractDAO;
import it.unimi.dsi.fastutil.longs.Long2DoubleOpenHashMap;
import org.hibernate.SessionFactory;

import java.util.List;

public class RatingDAO extends AbstractDAO<Rating> {

    public RatingDAO(SessionFactory factory) {
        super(factory);
    }

    public Rating createOrUpdate(Module module, User user, double rating, long timestamp) {
        Rating existingRating = findOne(user, module);
        if (existingRating != null) {
            existingRating.setRating(rating);
            existingRating.setTimestamp(timestamp);
            return existingRating;
        } else {
            Rating newRating = new Rating(module, user, rating, timestamp);
            return persist(newRating);
        }
    }

    public Rating findOne(User user, Module module) {
        List<Rating> list = namedQuery("Rating.findOneByItemAndUser").setParameter("user", user).setParameter("module", module).list();
        return list.isEmpty() ? null : list.get(0);
    }

    public List<Rating> findRatingsByUser(User user) {
        List<Rating> list = namedQuery("Rating.findRatingsByUser").setParameter("user", user).list();
        return list;
    }

    public List<Rating> findRatingsByModule(Module module) {
        return namedQuery("Rating.findRatingsByModule").setParameter("module", module).list();
    }

    public void deleteRating(User user, Module module) {
        namedQuery("Rating.deleteRating").setParameter("user", user).setParameter("module", module).executeUpdate();
    }

    public double getAverageRating(Module module) {
        List<Object[]> list = namedQuery("Rating.getAverageRating").setParameter("module", module).list();
        return list.isEmpty() ? null : (double) list.get(0)[0];
    }

    public Long2DoubleOpenHashMap getAverageRatings(long minCount) {
        List<Object[]> list = namedQuery("Rating.getAverageRatings").setParameter("minCount", minCount).list();
        Long2DoubleOpenHashMap averageRatings = new Long2DoubleOpenHashMap();
        for (Object[] item : list) {
            double avgRating = (double) item[0];
            long moduleId = (long) item[1];
            averageRatings.put(moduleId, avgRating);
        }
        return averageRatings;
    }

    public long getRatingsCount(Module module) {
        return (long) namedQuery("Rating.numberOfRatingsByModule").setParameter("module", module).list().get(0);
    }

    public long getRatingsCount() {
        return (long) namedQuery("Rating.numberOfRatings").list().get(0);
    }

    public long getTimestampOfRatingCount(long ratingCount) {
        List<Long> list = namedQuery("Rating.ratingsOrderedByTimestamp").list();
        return list.get((int) ratingCount - 1);
    }

    public boolean userRatingsExist(User user, long timestamp) {
        long numberOfRatings = (long) namedQuery("Rating.userRatingCount").setParameter("user", user).setParameter("timestamp", timestamp).list().get(0);
        return numberOfRatings > 0;
    }
}