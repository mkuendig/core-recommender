package ch.uzh.ifi.core.data.module;

import ch.uzh.ifi.core.ApplicationMain;
import io.dropwizard.hibernate.AbstractDAO;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleDAO extends AbstractDAO<Module> {

    private final WeightedFeatureDAO weightedFeatureDAO;

    public ModuleDAO(WeightedFeatureDAO weightedFeatureDAO) {
        super(ApplicationMain.getSessionFactory());
        this.weightedFeatureDAO = weightedFeatureDAO;
    }

    public Module create(String meteorModuleId) {
        Module module = new Module(meteorModuleId);
        return persist(module);
    }

    public Module persistModule(Module module) {
        return persist(module);
    }

    public void createWeightedFeature(Module module, String keyword, double weight) {
        weightedFeatureDAO.create(module, keyword, weight);
    }

    public void clearWeightedFeatures() {
        weightedFeatureDAO.removeAllWeightedFeatures();
    }

    public Module findOneByMeteorId(String meteorModuleId) {
        List<Module> list = namedQuery("Module.findOneByMeteorModuleId").setString("meteorModuleId", meteorModuleId).list();
        return (list.isEmpty() ? null : list.get(0));
    }

    public Module findOneById(long id) {
        List<Module> list = namedQuery("Module.findOneByModuleId").setLong("id", id).list();
        return (list.isEmpty() ? null : list.get(0));
    }

    public List<Module> findAllModules() {
        return namedQuery("Module.findAllModules").list();
    }

    public List<Long> findAllModuleIds() {
        return namedQuery("Module.findAllModuleIds").list();
    }

    public Long2ObjectMap<Module> getModules() {
        Long2ObjectMap<Module> modulesMap = new Long2ObjectOpenHashMap<Module>(1200);
        for (Module module : findAllModules()) {
            modulesMap.put(module.getId(), module);
        }
        return modulesMap;
    }

    public long getModulesCount() {
        return (long) namedQuery("Module.numberOfModules").list().get(0);
    }

    public long getWeightedFeaturesCount() {
        return weightedFeatureDAO.getWeightedFeaturesCount();
    }

    public Map<String, Module> getModulesMap() {
        HashMap<String, Module> moduleMap = new HashMap<String, Module>();
        List<Module> modulesList = findAllModules();
        for (Module module : modulesList) {
            moduleMap.put(module.getMeteorModuleId(), module);
        }
        return moduleMap;
    }

    public void saveModules(Collection<Module> modules) {
        for (Module module : modules) {
            persist(module);
        }
    }

}
