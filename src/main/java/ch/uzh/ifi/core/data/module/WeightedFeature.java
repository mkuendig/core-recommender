package ch.uzh.ifi.core.data.module;


import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = "WeightedFeature.findAll",
                query = "SELECT wf FROM WeightedFeature wf"
        ),
        @NamedQuery(
                name = "WeightedFeature.deleteAll",
                query = "DELETE FROM WeightedFeature wf"
        ),
        @NamedQuery(
                name = "WeightedFeature.numberOfWeightedFeatures",
                query = "SELECT COUNT(wf) FROM WeightedFeature wf"
        )
})

@Entity
@Table(name = "weighted_features")
public class WeightedFeature {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "module_id")
    private Module module;

    @Column(name = "weight")
    private double weight;

    @Column(name = "name")
    String name;

    public WeightedFeature() {
    }

    public WeightedFeature(Module module, String name, double weight) {
        this.module = module;
        this.weight = weight;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public void setModule(Module module) {
        this.module = module;
    }
}
