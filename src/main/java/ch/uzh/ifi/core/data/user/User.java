package ch.uzh.ifi.core.data.user;

import ch.uzh.ifi.core.data.module.Module;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NamedQueries({
        @NamedQuery(
                name = "User.findOneByMeteorUserId",
                query = "SELECT u FROM User u WHERE u.meteorUserId=:meteorUserId"
        ),
        @NamedQuery(
                name = "User.findOneByUserId",
                query = "SELECT u FROM User u WHERE u.id=:id"
        ),
        @NamedQuery(
                name = "User.findAllUsers",
                query = "SELECT u FROM User u"
        ),
        @NamedQuery(
                name = "User.findAllUserIds",
                query = "SELECT u.id From User u"
        ),
        @NamedQuery(
                name = "User.findNotInterestedModules",
                query = "SELECT u.notInterestedModules FROM User u WHERE u.id=:id"
        ),
        @NamedQuery(
                name = "User.numberOfUsers",
                query = "SELECT COUNT(u) FROM User u"
        )
})

@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id;

    @Column(name = "meteor_user_id", nullable = false)
    private String meteorUserId;

    @Column(name = "study_course", nullable = true)
    private String studyCourse;

    @OneToMany
    @JoinTable(name = "not_interested", joinColumns = {
            @JoinColumn(name = "user_id")}, inverseJoinColumns = {
            @JoinColumn(name = "module_id")})
    private List<Module> notInterestedModules = new ArrayList<>();

    public String getStudyCourse() {
        return studyCourse;
    }

    public void setStudyCourse(String studyCourse) {
        this.studyCourse = studyCourse;
    }

    public long getId() {
        return id;
    }

    public String getMeteorUserId() {
        return meteorUserId;
    }

    public void setMeteorUserId(String meteorUserId) {
        this.meteorUserId = meteorUserId;
    }

    public void addNotInterestedModule(Module module) {
        notInterestedModules.add(module);
    }
}