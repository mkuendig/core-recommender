package ch.uzh.ifi.core.data.module;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

public class WeightedFeatureDAO extends AbstractDAO<WeightedFeature> {


    public WeightedFeatureDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public WeightedFeature create(Module module, String name, double weight) {
        WeightedFeature weightedFeature = new WeightedFeature(module, name, weight);
        return persist(weightedFeature);
    }

    public void removeAllWeightedFeatures() {
        namedQuery("WeightedFeature.deleteAll").executeUpdate();
    }

    public long getWeightedFeaturesCount() {
        return (long) namedQuery("WeightedFeature.numberOfWeightedFeatures").list().get(0);
    }
}
