package ch.uzh.ifi.core.data.rating;

import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.user.User;
import org.grouplens.lenskit.data.event.Event;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({
        @NamedQuery(
                name = "Rating.findOneByItemAndUser",
                query = "SELECT r FROM Rating r WHERE r.module=:module AND r.user=:user"
        ),
        @NamedQuery(
                name = "Rating.findRatingsByUser",
                query = "SELECT r FROM Rating r WHERE r.user=:user ORDER BY r.module"
        ),
        @NamedQuery(
                name = "Rating.userRatingCount",
                query = "SELECT COUNT(r) FROM Rating r WHERE r.user=:user AND r.timestamp<:timestamp"
        ),
        @NamedQuery(
                name = "Rating.findRatingsByModule",
                query = "SELECT r FROM Rating r WHERE r.module=:module"
        ),
        @NamedQuery(
                name = "Rating.getAverageRating",
                query = "SELECT AVG(r.rating), r.module.id FROM Rating r WHERE r.module=:module GROUP BY r.module"
        ),
        @NamedQuery(
                name = "Rating.getAverageRatings",
                query = "SELECT AVG(r.rating), r.module.id FROM Rating r GROUP BY r.module HAVING COUNT(r.rating) > :minCount"
        ),
        @NamedQuery(
                name = "Rating.deleteRating",
                query = "DELETE FROM Rating r WHERE r.user=:user AND r.module=:module"
        ),
        @NamedQuery(
                name = "Rating.numberOfRatings",
                query = "SELECT COUNT(r) FROM Rating r"
        ),
        @NamedQuery(
                name = "Rating.numberOfRatingsByModule",
                query = "SELECT COUNT(r) FROM Rating r WHERE r.module=:module"
        ),
        @NamedQuery(
                name = "Rating.ratingsOrderedByTimestamp",
                query = "SELECT r.timestamp from Rating r ORDER BY r.timestamp ASC"
        )
})

@Entity
@Table(name = "ratings")
public class Rating implements Event, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "module_id")
    private Module module;

    @Column(name = "rating", nullable = false)
    private double rating;

    @Column(name = "timestamp", nullable = false)
    private long timestamp;

    public Rating() {
    }

    public Rating(Module module, User user, double rating, long timestamp) {
        this.module = module;
        this.user = user;
        this.rating = rating;
        this.timestamp = timestamp;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public long getUserId() {
        return user.getId();
    }

    public long getItemId() {
        return module.getId();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}