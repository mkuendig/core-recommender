package ch.uzh.ifi.core.data.module;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


@NamedQueries({
        @NamedQuery(
                name = "Module.findOneByMeteorModuleId",
                query = "SELECT m FROM Module m WHERE m.meteorModuleId=:meteorModuleId"
        ),
        @NamedQuery(
                name = "Module.findOneByModuleId",
                query = "SELECT m FROM Module m WHERE m.id=:id"
        ),
        @NamedQuery(
                name = "Module.findAllModules",
                query = "SELECT m FROM Module m"
        ),
        @NamedQuery(
                name = "Module.findAllModuleIds",
                query = "SELECT m.id FROM Module m"
        ),
        @NamedQuery(
                name = "Module.numberOfModules",
                query = "SELECT COUNT(m) FROM Module m"
        )
})

@Entity
@Table(name = "modules")
public class Module implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id;

    @Column(name = "meteor_module_id", nullable = false)
    private String meteorModuleId;

    @Column(name = "module_title", nullable = false)
    private String moduleTitle;

    @OneToMany(mappedBy = "module")
    private Collection<WeightedFeature> weightedFeatures = new ArrayList<WeightedFeature>();

    public Module() {
    }

    public Module(String meteorModuleId) {
        this.meteorModuleId = meteorModuleId;
    }

    public long getId() {
        return id;
    }

    public String getMeteorModuleId() {
        return meteorModuleId;
    }

    protected void addWeightedFeature(WeightedFeature weightedFeature) {
        weightedFeature.setModule(this);
        weightedFeatures.add(weightedFeature);
    }

    public Collection<WeightedFeature> getWeightedFeatures() {
        return weightedFeatures;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }
}