package ch.uzh.ifi.core.data;

import ch.uzh.ifi.core.ApplicationMain;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.module.WeightedFeatureDAO;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.UserDAO;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.data.sql.SQLStatementFactory;
import org.hibernate.SessionFactory;

import java.sql.Connection;
import java.sql.DriverManager;

public class DAOBuilder {

    private static DAOBuilder instance;

    private static ModuleDAO moduleDAO;
    private static RatingDAO ratingDAO;
    private static UserDAO userDAO;
    private static JDBCRatingDAO jdbcRatingDAO;
    private static WeightedFeatureDAO weightedFeatureDAO;

    private DAOBuilder() {
        SessionFactory sessionFactory = ApplicationMain.getSessionFactory();
        ratingDAO = new RatingDAO(sessionFactory);
        userDAO = new UserDAO(sessionFactory);
        weightedFeatureDAO = new WeightedFeatureDAO(sessionFactory);
        moduleDAO = new ModuleDAO(weightedFeatureDAO);
        jdbcRatingDAO = buildJDBCRatingDAO();
    }

    public static DAOBuilder getInstance() {
        if (instance == null) {
            instance = new DAOBuilder();
        }
        return instance;
    }

    private JDBCRatingDAO buildJDBCRatingDAO() {
        Connection connection = getSQLConnection();
        return new JDBCRatingDAO(connection, new CustomSQLStatementFactory());
    }

    public JDBCRatingDAO buildJDBCRatingDAO(SQLStatementFactory statementFactory) {
        Connection connection = getSQLConnection();
        return new JDBCRatingDAO(connection, statementFactory);
    }

    private Connection getSQLConnection() {
        Connection connection = null;
        String DB_URL = "jdbc:postgresql://localhost/core_recommender";
        String USER = "mprec";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, "ardecinsilde");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public ModuleDAO getModuleDAO() {
        return moduleDAO != null ? moduleDAO : null;
    }

    public RatingDAO getRatingDAO() {
        return ratingDAO != null ? ratingDAO : null;
    }

    public UserDAO getUserDAO() {
        return userDAO != null ? userDAO : null;
    }

    public JDBCRatingDAO getJDBCRatingDAO() {
        return jdbcRatingDAO != null ? jdbcRatingDAO : null;
    }
}
