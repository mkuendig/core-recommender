package ch.uzh.ifi.core.data.user;

import ch.uzh.ifi.core.data.module.Module;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class UserDAO extends AbstractDAO<User> {

    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public User findOrCreate(String meteorUserId) {
        User existingUser = findOneByMeteorId(meteorUserId);
        if (existingUser != null) {
            return existingUser;
        } else {
            User newUser = new User();
            newUser.setMeteorUserId(meteorUserId);
            return persist(newUser);
        }
    }

    public User findOneByMeteorId(String meteorUserId) {
        List<User> list = namedQuery("User.findOneByMeteorUserId").setString("meteorUserId", meteorUserId).list();
        return list.isEmpty() ? null : list.get(0);
    }

    public User findOneByMeteorId(long id) {
        List<User> list = namedQuery("User.findOneByUserId").setLong("id", id).list();
        return list.isEmpty() ? null : list.get(0);
    }

    public List<User> findAllUsers() {
        List<User> list = namedQuery("User.findAllUsers").list();
        return list;
    }

    public List<Module> findNotInterestedModules(long id) {
        return namedQuery("User.findNotInterestedModules").setLong("id", id).list();
    }

    public List<Long> findAllUserIds() {
        return namedQuery("User.findAllUserIds").list();
    }

    public long getUsersCount() {
        return (long) namedQuery("User.numberOfUsers").list().get(0);
    }

    public void saveUser(User user) {
        persist(user);
    }
}