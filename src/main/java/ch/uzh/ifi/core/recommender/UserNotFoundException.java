package ch.uzh.ifi.core.recommender;

public class UserNotFoundException extends RuntimeException {

    long userId;

    public UserNotFoundException(long userId) {
        this.userId = userId;
    }
}
