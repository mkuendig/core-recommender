package ch.uzh.ifi.core.recommender.params;


import org.grouplens.lenskit.transform.normalize.MeanVarianceNormalizer;
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity;

public class EvalParams {

    /**
     * EvaluationResource class overwrites values from url parameters
     */

    public static Class VECTOR_SIMILARITY = CosineVectorSimilarity.class;
    public static Class VECTOR_NORMALIZER = MeanVarianceNormalizer.class;
    public static int NEIGHBORHOOD_SIZE = 10;
    public static int MIN_NEIGHBOR_COUNT = 1;
    public static double HYBRID_CF_PERCENTAGE = .6;

    public static int SIMILAR_MODULES_COUNT = 10;
    public static double SIMILAR_MODULE_DECREASE = .3;

    public static void resetValues() {
        VECTOR_SIMILARITY = CosineVectorSimilarity.class;
        VECTOR_NORMALIZER = MeanVarianceNormalizer.class;
        NEIGHBORHOOD_SIZE = 10;
        MIN_NEIGHBOR_COUNT = 1;
        HYBRID_CF_PERCENTAGE = .6;
        SIMILAR_MODULES_COUNT = 10;
        SIMILAR_MODULE_DECREASE = .3;
    }
}
