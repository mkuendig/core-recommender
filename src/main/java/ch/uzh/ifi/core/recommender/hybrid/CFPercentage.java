package ch.uzh.ifi.core.recommender.hybrid;

import org.grouplens.grapht.annotation.DefaultInteger;
import org.grouplens.lenskit.core.Parameter;

import javax.inject.Qualifier;
import java.lang.annotation.*;

/**
 * Number of neighbors to consider when building a prediction.  Used by both
 * user-user and item-item CF. If 0, then all neighbors are used.
 */
@Documented
@DefaultInteger(20)
@Parameter(Double.class)
@Qualifier
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CFPercentage {
}
