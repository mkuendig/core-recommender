package ch.uzh.ifi.core.recommender;

import ch.uzh.ifi.core.data.CustomSQLStatementFactory;
import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.recommender.cb.CBItemScorer;
import ch.uzh.ifi.core.recommender.hybrid.CFPercentage;
import ch.uzh.ifi.core.recommender.hybrid.HybridScorer;
import ch.uzh.ifi.core.recommender.params.RecParams;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.dao.EventDAO;
import org.grouplens.lenskit.data.dao.ItemDAO;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.MinNeighbors;
import org.grouplens.lenskit.knn.NeighborhoodSize;
import org.grouplens.lenskit.knn.user.UserUserItemScorer;
import org.grouplens.lenskit.knn.user.UserVectorSimilarity;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.VectorNormalizer;
import org.grouplens.lenskit.vectors.similarity.VectorSimilarity;

public class RecBuilder {

    private static ModuleDAO moduleDAO = DAOBuilder.getInstance().getModuleDAO();
    private static JDBCRatingDAO jdbcRatingDAO = DAOBuilder.getInstance().getJDBCRatingDAO();

    public static LenskitRecommender buildCBRecommender() throws RecommenderBuildException {
        LenskitConfiguration configuration = getLenskitConfiguration(jdbcRatingDAO);
        configuration.bind(ItemScorer.class).to(CBItemScorer.class);
        configuration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(RecParams.CB_VECTOR_SIMILARITY);
        configuration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(RecParams.CB_VECTOR_NORMALIZER);
        return LenskitRecommender.build(configuration);
    }

    public static LenskitRecommender buildCBRecommender(long timestamp, long userId) throws RecommenderBuildException {
        CustomSQLStatementFactory statementFactory = new CustomSQLStatementFactory();
        statementFactory.setMaxTimestamp(timestamp);
        statementFactory.setUserId(userId);
        JDBCRatingDAO jdbcRatingDAO = DAOBuilder.getInstance().buildJDBCRatingDAO(statementFactory);
        LenskitConfiguration configuration = getLenskitConfiguration(jdbcRatingDAO);
        configuration.bind(ItemScorer.class).to(CBItemScorer.class);
        configuration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(RecParams.CB_VECTOR_SIMILARITY);
        configuration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(RecParams.CB_VECTOR_NORMALIZER);
        return LenskitRecommender.build(configuration);
    }

    public static LenskitRecommender buildCFRecommender() throws RecommenderBuildException {
        LenskitConfiguration configuration = getLenskitConfiguration(jdbcRatingDAO);
        configuration.bind(ItemScorer.class).to(UserUserItemScorer.class);
        configuration.set(NeighborhoodSize.class).to(RecParams.CF_NEIGHBORHOOD_SIZE);
        configuration.set(MinNeighbors.class).to(RecParams.CF_MIN_NEIGHBOR_COUNT);
        configuration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(RecParams.CF_VECTOR_SIMILARITY);
        configuration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(RecParams.CF_VECTOR_NORMALIZER);
        return LenskitRecommender.build(configuration);
    }

    public static Recommender buildCFRecommender(long timestamp, long userId) throws RecommenderBuildException {
        CustomSQLStatementFactory statementFactory = new CustomSQLStatementFactory();
        statementFactory.setMaxTimestamp(timestamp);
        statementFactory.setUserId(userId);
        JDBCRatingDAO jdbcRatingDAO = DAOBuilder.getInstance().buildJDBCRatingDAO(statementFactory);
        LenskitConfiguration configuration = getLenskitConfiguration(jdbcRatingDAO);
        configuration.bind(ItemScorer.class).to(UserUserItemScorer.class);
        configuration.set(NeighborhoodSize.class).to(RecParams.CF_NEIGHBORHOOD_SIZE);
        configuration.set(MinNeighbors.class).to(RecParams.CF_MIN_NEIGHBOR_COUNT);
        configuration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(RecParams.CF_VECTOR_SIMILARITY);
        configuration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(RecParams.CF_VECTOR_NORMALIZER);
        return LenskitRecommender.build(configuration);
    }

    public static LenskitRecommender buildHybridRecommender() throws RecommenderBuildException {
        LenskitConfiguration configuration = getLenskitConfiguration(jdbcRatingDAO);
        configuration.bind(ItemScorer.class).to(HybridScorer.class);
        configuration.set(NeighborhoodSize.class).to(RecParams.HYBRID_NEIGHBORHOOD_SIZE);
        configuration.set(MinNeighbors.class).to(RecParams.HYBRID_MIN_NEIGHBOR_COUNT);
        configuration.set(CFPercentage.class).to(RecParams.HYBRID_CF_PERCENTAGE);
        configuration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(RecParams.HYBRID_VECTOR_SIMILARITY);
        configuration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(RecParams.HYBRID_VECTOR_NORMALIZER);
        return LenskitRecommender.build(configuration);
    }

    private static LenskitConfiguration getLenskitConfiguration(JDBCRatingDAO jdbcRatingDAO) {
        LenskitConfiguration configuration = new LenskitConfiguration();
        configuration.bind(ModuleDAO.class).to(moduleDAO);
        configuration.bind(ItemDAO.class).to(jdbcRatingDAO);
        configuration.bind(EventDAO.class).to(jdbcRatingDAO);
        return configuration;
    }
}
