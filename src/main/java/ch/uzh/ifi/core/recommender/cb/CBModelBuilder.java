package ch.uzh.ifi.core.recommender.cb;


import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.module.WeightedFeature;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import it.unimi.dsi.fastutil.longs.Long2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.SparseVector;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CBModelBuilder implements Provider<CBModel> {

    ModuleDAO moduleDAO;
    RatingDAO ratingDAO;

    @Inject
    public CBModelBuilder() {
        this.moduleDAO = DAOBuilder.getInstance().getModuleDAO();
        this.ratingDAO = DAOBuilder.getInstance().getRatingDAO();
    }

    public CBModel get() {
        List<Module> modules = moduleDAO.findAllModules();
        Map<Long, SparseVector> modulesVector = getModuleVectors(modules);
        Map<String, Long> attributeIds = getAttributes(modules);
        Long2DoubleOpenHashMap averageRatings = ratingDAO.getAverageRatings(3);
        return new CBModel(modulesVector, attributeIds, averageRatings);
    }

    public Long2ObjectMap getModuleVectors(List<Module> modules) {
        Long2ObjectOpenHashMap moduleVectors = new Long2ObjectOpenHashMap();
        Map<String, Long> attributeIds = getAttributes(modules);
        for (Module module : modules) {
            MutableSparseVector moduleVector = MutableSparseVector.create(attributeIds.values());
            moduleVector.fill(0);
            long attrId;
            for (WeightedFeature weightedFeature : module.getWeightedFeatures()) {
                attrId = attributeIds.get(weightedFeature.getName());
                moduleVector.add(attrId, weightedFeature.getWeight());
            }
            moduleVectors.put(module.getId(), moduleVector.shrinkDomain());
        }
        return moduleVectors;
    }

    public Map<String, Long> getAttributes(List<Module> modules) {
        HashMap<String, Long> attributeIds = new HashMap<String, Long>();
        long attributeId = 0;
        for (Module module : modules) {
            for (WeightedFeature weightedFeature : module.getWeightedFeatures()) {
                String name = weightedFeature.getName();
                if (!attributeIds.containsKey(name)) {
                    attributeIds.put(name, attributeId);
                    attributeId++;
                }
            }
        }
        return attributeIds;
    }
}