package ch.uzh.ifi.core.recommender.hybrid;

import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.Module;
import ch.uzh.ifi.core.data.user.UserDAO;
import ch.uzh.ifi.core.recommender.cb.CBItemScorer;
import ch.uzh.ifi.core.recommender.params.RecParams;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.basic.AbstractItemScorer;
import org.grouplens.lenskit.knn.user.UserUserItemScorer;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.VectorEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class HybridScorer extends AbstractItemScorer implements ItemScorer {
    private static final Logger logger = LoggerFactory.getLogger(HybridScorer.class);
    private final CBItemScorer cbItemScorer;
    private final ItemScorer cfItemScorer;
    private final UserDAO userDAO;
    private final double cfPercentage;


    @Inject
    public HybridScorer(CBItemScorer cbItemScorer, UserUserItemScorer cfItemScorer, @CFPercentage double cfPercentage) {
        this.cbItemScorer = cbItemScorer;
        this.cfItemScorer = cfItemScorer;
        this.userDAO = DAOBuilder.getInstance().getUserDAO();
        this.cfPercentage = cfPercentage;
    }

    @Override
    public void score(long userId, @Nonnull MutableSparseVector scores) {
        MutableSparseVector cbScores = scores.copy();
        MutableSparseVector cfScores = scores.copy();
        cfItemScorer.score(userId, cfScores);
        cbItemScorer.score(userId, cbScores);

        for (VectorEntry scoreEntry : scores.view(VectorEntry.State.EITHER)) {
            long moduleId = scoreEntry.getKey();
            double cfScore = cfScores.containsKey(moduleId) ? cfScores.get(moduleId) : 0;
            double cbScore = cbScores.containsKey(moduleId) ? cbScores.get(moduleId) : 0;
            double hybridScore = (cfPercentage * cfScore) + ((1 - cfPercentage) * cbScore);
            double score;

            if (cfScore > 0 && cbScore > 0) {
                score = hybridScore;
            } else {
                score = cbScore;
            }

//            logger.info("----");
//            logger.info("Avg Rating: {}", cbItemScorer.getAverageRating(moduleId));
//            logger.info("Rating count: {}", ratingCount);
//            logger.info("CFScore: {}", cfScore);
//            logger.info("CBScore: {}", cbScore);
//            logger.info("HyScore: {}", hybridScore);
//            logger.info("----");
            scores.set(moduleId, score);
        }

//        List<Module> notInterestedModules = userDAO.findNotInterestedModules(userId);
//        for (Module module : notInterestedModules) {
//            int counter = 0;
//            Collection<Long> similarModules = cbItemScorer.getSimilarModules(module.getId()).values();
//            Iterator iterator = similarModules.iterator();
//            while (iterator.hasNext() && counter < RecParams.HYBRID_SIMILAR_MODULES_COUNT) {
//                counter++;
//                long similarModule = (Long) iterator.next();
//                if (scores.containsKey(similarModule)) {
//                    scores.set(similarModule, scores.get(similarModule) - RecParams.HYBRID_SIMILAR_MODULE_DECREASE);
//                }
//            }
//        }

        logger.info("number hybrid scores calculated: {}", scores.size());
    }
}
