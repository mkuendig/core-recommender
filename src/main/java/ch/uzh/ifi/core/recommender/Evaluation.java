package ch.uzh.ifi.core.recommender;

import ch.uzh.ifi.core.data.CustomSQLStatementFactory;
import ch.uzh.ifi.core.data.DAOBuilder;
import ch.uzh.ifi.core.data.module.ModuleDAO;
import ch.uzh.ifi.core.data.rating.RatingDAO;
import ch.uzh.ifi.core.data.user.UserDAO;
import ch.uzh.ifi.core.recommender.cb.CBItemScorer;
import ch.uzh.ifi.core.recommender.hybrid.CFPercentage;
import ch.uzh.ifi.core.recommender.hybrid.HybridScorer;
import ch.uzh.ifi.core.recommender.params.EvalParams;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.io.FileUtils;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.baseline.*;
import org.grouplens.lenskit.core.LenskitConfiguration;
import org.grouplens.lenskit.core.LenskitRecommender;
import org.grouplens.lenskit.data.dao.EventDAO;
import org.grouplens.lenskit.data.dao.ItemDAO;
import org.grouplens.lenskit.data.dao.UserEventDAO;
import org.grouplens.lenskit.data.source.GenericDataSource;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.eval.TaskExecutionException;
import org.grouplens.lenskit.eval.algorithm.AlgorithmInstance;
import org.grouplens.lenskit.eval.data.crossfold.CrossfoldMethod;
import org.grouplens.lenskit.eval.data.crossfold.CrossfoldTask;
import org.grouplens.lenskit.eval.metrics.predict.CoveragePredictMetric;
import org.grouplens.lenskit.eval.metrics.predict.MAEPredictMetric;
import org.grouplens.lenskit.eval.metrics.predict.NDCGPredictMetric;
import org.grouplens.lenskit.eval.metrics.predict.RMSEPredictMetric;
import org.grouplens.lenskit.eval.traintest.SimpleEvaluator;
import org.grouplens.lenskit.iterative.IterationCount;
import org.grouplens.lenskit.iterative.LearningRate;
import org.grouplens.lenskit.knn.MinNeighbors;
import org.grouplens.lenskit.knn.NeighborhoodSize;
import org.grouplens.lenskit.knn.item.ItemItemScorer;
import org.grouplens.lenskit.knn.item.ItemVectorSimilarity;
import org.grouplens.lenskit.knn.user.UserUserItemScorer;
import org.grouplens.lenskit.knn.user.UserVectorSimilarity;
import org.grouplens.lenskit.mf.funksvd.FeatureCount;
import org.grouplens.lenskit.mf.funksvd.FunkSVDItemScorer;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.ItemVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.VectorNormalizer;
import org.grouplens.lenskit.util.table.Row;
import org.grouplens.lenskit.util.table.Table;
import org.grouplens.lenskit.vectors.similarity.VectorSimilarity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.SortedMap;

public class Evaluation {

    private static final Logger logger = LoggerFactory.getLogger(Evaluation.class);
    private final int NUMBER_OF_PARTITIONS = 10;
    private LenskitConfiguration defaultConfiguration;
    private SimpleEvaluator simpleEvaluator;
    private JDBCRatingDAO jdbcRatingDAO;

    public Evaluation() {
        this(null, 0);
    }

    public Evaluation(long timestamp) {
        this(null, timestamp);
    }

    public Evaluation(String studyCourse) {
        this(studyCourse, 0);
    }

    public Evaluation(String studyCourse, long timestamp) {
        CustomSQLStatementFactory statementFactory = new CustomSQLStatementFactory();
        if (studyCourse != null) {
            statementFactory.setStudyCourse(studyCourse);
        }
        if (timestamp > 0) {
            statementFactory.setMaxTimestamp(timestamp);
        }

        jdbcRatingDAO = DAOBuilder.getInstance().buildJDBCRatingDAO(statementFactory);
        defaultConfiguration = getDefaultConfiguration();
        simpleEvaluator = new SimpleEvaluator();
        simpleEvaluator.setOutputPath("evaluation_result.csv");
        simpleEvaluator.addMetric(new RMSEPredictMetric());
        simpleEvaluator.addMetric(new MAEPredictMetric());
        simpleEvaluator.addMetric(new CoveragePredictMetric());
        simpleEvaluator.addMetric(new NDCGPredictMetric());
    }

    public List<EvaluationResult> getCBEvaluationResultList() throws IOException, TaskExecutionException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(CBItemScorer.class);
        AlgorithmInstance cbAlgorithm = new AlgorithmInstance("CBItemScorer ", config);
        simpleEvaluator.addAlgorithm(cbAlgorithm);
        return buildResultList();
    }

    public List<EvaluationResult> getUserUserEvaluationResultList() throws TaskExecutionException, IOException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(UserUserItemScorer.class);
        String description = "User-User Collaborative Filtering - Neighbourhood Size: " + EvalParams.NEIGHBORHOOD_SIZE +
                ", Similarity: " + EvalParams.VECTOR_SIMILARITY;
        AlgorithmInstance userAlgorithm = new AlgorithmInstance(description, config);
        simpleEvaluator.addAlgorithm(userAlgorithm);
        return buildResultList();
    }

    public List<EvaluationResult> getItemItemEvaluationResultList() throws TaskExecutionException, IOException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(ItemItemScorer.class);
        String description = "Item-Item Collaborative Filtering - Neighbourhood Size: " + EvalParams.NEIGHBORHOOD_SIZE +
                ", Similarity: " + EvalParams.VECTOR_SIMILARITY;
        AlgorithmInstance itemAlgorithm = new AlgorithmInstance(description, config);
        simpleEvaluator.addAlgorithm(itemAlgorithm);
        return buildResultList();
    }


    public List<EvaluationResult> getHybridEvaluationResultList() throws IOException, TaskExecutionException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(HybridScorer.class);
        config.set(CFPercentage.class).to(EvalParams.HYBRID_CF_PERCENTAGE);
        AlgorithmInstance hybridAlgorithm = new AlgorithmInstance("HybridScorer - Neighbourhood Size " + EvalParams.NEIGHBORHOOD_SIZE, config);
        simpleEvaluator.addAlgorithm(hybridAlgorithm);
        return buildResultList();
    }

    public List<EvaluationResult> getUserConstantEvaluationResultList(double constant) throws IOException, TaskExecutionException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(new ConstantItemScorer(constant));
        AlgorithmInstance userMeanItemAlgorithm = new AlgorithmInstance("Constant Scorer of " + constant, config);
        simpleEvaluator.addAlgorithm(userMeanItemAlgorithm);
        return buildResultList();
    }


    public List<EvaluationResult> getUserMeanEvaluationResultList() throws IOException, TaskExecutionException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(UserMeanItemScorer.class);
        AlgorithmInstance userMeanItemAlgorithm = new AlgorithmInstance("Global Mean Scorer", config);
        simpleEvaluator.addAlgorithm(userMeanItemAlgorithm);
        return buildResultList();
    }

    public List<EvaluationResult> getFunkSVDEvaluationResultList() throws IOException, TaskExecutionException {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(FunkSVDItemScorer.class);
        config.bind(UserVectorNormalizer.class).to(BaselineSubtractingUserVectorNormalizer.class);
        config.bind(BaselineScorer.class, ItemScorer.class).to(ItemMeanRatingItemScorer.class);
        config.set(FeatureCount.class).to(40);
        config.set(LearningRate.class).to(0.001);
        config.set(IterationCount.class).to(200);
        AlgorithmInstance funkSVDItemAlgorithm = new AlgorithmInstance("FunkSVD", config);
        simpleEvaluator.addAlgorithm(funkSVDItemAlgorithm);
        return buildResultList();
    }

    public SortedMap<Double, Long> findSimilarModules(long id) {
        LenskitConfiguration config = new LenskitConfiguration(defaultConfiguration);
        config.bind(ItemScorer.class).to(CBItemScorer.class);
        Recommender rec = null;
        SortedMap<Double, Long> result = null;
        try {
            rec = LenskitRecommender.build(config);
        } catch (RecommenderBuildException e) {
            e.printStackTrace();
        }
        if (rec.getItemScorer() instanceof CBItemScorer) {
            CBItemScorer itemScorer = (CBItemScorer) rec.getItemScorer();
            result = itemScorer.getSimilarModules(id);
        }
        return result;
    }

    private LenskitConfiguration getDefaultConfiguration() {
        DAOBuilder daoBuilder = DAOBuilder.getInstance();
        LenskitConfiguration defaultConfiguration = new LenskitConfiguration();
        defaultConfiguration.bind(ItemDAO.class).to(jdbcRatingDAO);
        defaultConfiguration.bind(EventDAO.class).to(jdbcRatingDAO);
        defaultConfiguration.bind(UserEventDAO.class).to(jdbcRatingDAO);
        defaultConfiguration.bind(ModuleDAO.class).to(daoBuilder.getModuleDAO());
        defaultConfiguration.bind(UserDAO.class).to(daoBuilder.getUserDAO());
        defaultConfiguration.bind(RatingDAO.class).to(daoBuilder.getRatingDAO());
        defaultConfiguration.within(ItemVectorSimilarity.class).bind(VectorSimilarity.class).to(EvalParams.VECTOR_SIMILARITY);
        defaultConfiguration.within(ItemVectorNormalizer.class).bind(VectorNormalizer.class).to(EvalParams.VECTOR_NORMALIZER);
        defaultConfiguration.within(UserVectorSimilarity.class).bind(VectorSimilarity.class).to(EvalParams.VECTOR_SIMILARITY);
        defaultConfiguration.within(UserVectorNormalizer.class).bind(VectorNormalizer.class).to(EvalParams.VECTOR_NORMALIZER);
        defaultConfiguration.set(NeighborhoodSize.class).to(EvalParams.NEIGHBORHOOD_SIZE);
        defaultConfiguration.set(MinNeighbors.class).to(EvalParams.MIN_NEIGHBOR_COUNT);
        return defaultConfiguration;
    }

    public static void resetEvaluation() throws IOException {
        EvalParams.resetValues();
        String crossfoldFilesDir = "ratings-crossfold";
//        logger.info("deleting directory {}", crossfoldFilesDir);
//        FileUtils.deleteDirectory(new File(crossfoldFilesDir));
    }

    private List<EvaluationResult> buildResultList() throws TaskExecutionException, IOException {
        CrossfoldTask cross = new CrossfoldTask("ratings")
                .setSource(new GenericDataSource("ratings", jdbcRatingDAO))
                .setPartitions(NUMBER_OF_PARTITIONS)
                .setMethod(CrossfoldMethod.PARTITION_USERS)
                .setHoldoutFraction(.2);
        simpleEvaluator.addDataset(cross);
        Table resultTable = simpleEvaluator.call();

        DoubleSummaryStatistics rmseSummary = new DoubleSummaryStatistics();
        DoubleSummaryStatistics maeSummary = new DoubleSummaryStatistics();
        DoubleSummaryStatistics nDCGSummary = new DoubleSummaryStatistics();
        for (Row row : resultTable) {
            if (row.value("RMSE.ByRating") != null) {
                double rmse = (double) row.value("RMSE.ByRating");
                rmseSummary.accept(rmse);
            }
            if (row.value("MAE.ByRating") != null) {
                double mae = (double) row.value("MAE.ByRating");
                maeSummary.accept(mae);
            }
            if (row.value("nDCG") != null) {
                double nDCG = (double) row.value("nDCG");
                nDCGSummary.accept(nDCG);
            }
        }

        ArrayList<EvaluationResult> results = new ArrayList<>();
        results.add(new EvaluationResult("RMSE", rmseSummary.getAverage()));
        results.add(new EvaluationResult("MAE", maeSummary.getAverage()));
        results.add(new EvaluationResult("nDCG", nDCGSummary.getAverage()));

        resetEvaluation();
        return results;
    }

    public class EvaluationResult {
        private String name;
        private double result;

        public EvaluationResult(String name, double result) {
            this.name = name;
            this.result = result;
        }

        @JsonProperty
        public String getName() {
            return name;
        }

        @JsonProperty
        public double getResult() {
            return result;
        }
    }
}
