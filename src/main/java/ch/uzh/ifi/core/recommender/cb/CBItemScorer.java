package ch.uzh.ifi.core.recommender.cb;

import ch.uzh.ifi.core.recommender.UserNotFoundException;
import org.grouplens.lenskit.basic.AbstractItemScorer;
import org.grouplens.lenskit.data.dao.UserEventDAO;
import org.grouplens.lenskit.data.event.Rating;
import org.grouplens.lenskit.data.history.History;
import org.grouplens.lenskit.data.history.RatingVectorUserHistorySummarizer;
import org.grouplens.lenskit.data.history.UserHistory;
import org.grouplens.lenskit.knn.item.ItemSimilarity;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.grouplens.lenskit.vectors.ImmutableSparseVector;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.SparseVector;
import org.grouplens.lenskit.vectors.VectorEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Collections;
import java.util.DoubleSummaryStatistics;
import java.util.SortedMap;
import java.util.TreeMap;


public class CBItemScorer extends AbstractItemScorer {

    private final Logger logger = LoggerFactory.getLogger(CBItemScorer.class);
    private final CBModel cbModel;
    private final UserEventDAO userEventDAO;
    private ItemSimilarity itemSimilarity;
    private UserVectorNormalizer normalizer;

    @Inject
    public CBItemScorer(CBModel cbModel, UserEventDAO userEventDAO, ItemSimilarity itemSimilarity, UserVectorNormalizer normalizer) {
        this.cbModel = cbModel;
        this.userEventDAO = userEventDAO;
        this.itemSimilarity = itemSimilarity;
        this.normalizer = normalizer;
    }

    public void score(long userId, MutableSparseVector scores) throws UserNotFoundException {
        UserHistory<Rating> history = userEventDAO.getEventsForUser(userId, Rating.class);
        if (history == null) {
            throw new UserNotFoundException(userId);
        }
        SparseVector userVector = getUserVector(history);
        double avgScore = getAvgScore(history);
        for (VectorEntry entry : scores.view(VectorEntry.State.EITHER)) {
            long moduleId = entry.getKey();
            double prediction = getPrediction(userId, avgScore, userVector, moduleId);
            scores.set(moduleId, prediction);
        }
    }

    private double getPrediction(long userId, double avgScore, SparseVector userVector, long moduleId) {
        SparseVector moduleVector = cbModel.getModuleVector(moduleId);
        double similarity = itemSimilarity.similarity(userId, userVector, moduleId, moduleVector);
        double prediction;
        if (similarity > 0) {
            prediction = avgScore + (5 - avgScore) * similarity;
        } else {
            prediction = avgScore + (avgScore - 1) * similarity;
        }
        return prediction > 5.0 ? 5.0 : prediction;
    }

    private SparseVector getUserVector(UserHistory<Rating> history) {
        SparseVector urs = RatingVectorUserHistorySummarizer.makeRatingVector(history);
        ImmutableSparseVector normalizedRatings = normalizer.normalize(history.getUserId(), urs, null).freeze();
        MutableSparseVector userVector = cbModel.createFeaturesVector();
        for (Rating rating : history) { // go through all modules rated by the user and calculate the user features based on the rating and the feature value of the module
            double multiplier = normalizedRatings.get(rating.getItemId());
            SparseVector moduleVector = cbModel.getModuleVector(rating.getItemId());
            for (VectorEntry feature : moduleVector.view(VectorEntry.State.SET)) {
                long featureId = feature.getKey();
                double featureValue = multiplier * feature.getValue();
                userVector.set(featureId, featureValue + userVector.get(featureId));
            }
        }
        ImmutableSparseVector freezed = userVector.freeze();
        return freezed;
    }

    private double getAvgScore(UserHistory<Rating> history) {
        DoubleSummaryStatistics ratings = new DoubleSummaryStatistics();
        for (Rating rating : history) {
            ratings.accept(rating.getValue());
        }
        return ratings.getAverage();
    }

    public SortedMap<Double, Long> getSimilarModules(long moduleId) {
        SparseVector moduleVector = cbModel.getModuleVector(moduleId);
        SortedMap<Double, Long> similarModules = new TreeMap<Double, Long>(Collections.<Double>reverseOrder());
        for (Long itemId : cbModel.getItemIds()) {
            SparseVector compareVector = cbModel.getModuleVector(itemId);
            double similarity = itemSimilarity.similarity(moduleId, moduleVector, itemId, compareVector);
            if (itemId != moduleId && !Double.isNaN(similarity)) {
                similarModules.put(similarity, itemId);
            }
        }
        return similarModules;
    }
}
