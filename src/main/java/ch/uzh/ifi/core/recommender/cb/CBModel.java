package ch.uzh.ifi.core.recommender.cb;

import it.unimi.dsi.fastutil.longs.Long2DoubleOpenHashMap;
import org.grouplens.grapht.annotation.DefaultProvider;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.SparseVector;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

@DefaultProvider(CBModelBuilder.class)
public class CBModel implements Serializable {

    private final Map<Long, SparseVector> moduleFeaturesVectors;
    private final Map<String, Long> featureIds;
    private final Long2DoubleOpenHashMap averageRatings;

    public CBModel(Map<Long, SparseVector> moduleFeaturesVectors, Map<String, Long> featureIds, Long2DoubleOpenHashMap averageRatings) {
        this.moduleFeaturesVectors = moduleFeaturesVectors;
        this.featureIds = featureIds;
        this.averageRatings = averageRatings;
    }

    public MutableSparseVector createFeaturesVector() {
        MutableSparseVector userFeaturesVector = MutableSparseVector.create(featureIds.values());
        userFeaturesVector.fill(0);
        return userFeaturesVector;
    }

    public SparseVector getModuleVector(long item) {
        SparseVector vec = moduleFeaturesVectors.get(item);
        if (vec == null) {
            return SparseVector.empty();
        } else {
            return vec;
        }
    }

    public Collection<Long> getItemIds() {
        return moduleFeaturesVectors.keySet();
    }

    public double getAverageRating(long moduleId) {
        return averageRatings.get(moduleId);
    }


}
