package ch.uzh.ifi.core.recommender.params;

import org.grouplens.lenskit.knn.user.UserUserItemScorer;
import org.grouplens.lenskit.transform.normalize.MeanVarianceNormalizer;
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity;
import org.grouplens.lenskit.vectors.similarity.PearsonCorrelation;

public class RecParams {


    // HYBRID Recommender
    // percentage of cf scorer in prediction of hybrid scorer
    public final static double HYBRID_CF_PERCENTAGE = 0.61;

    public final static int HYBRID_NEIGHBORHOOD_SIZE = 10;

    public final static int HYBRID_MIN_NEIGHBOR_COUNT = 1;

    public final static Class HYBRID_VECTOR_SIMILARITY = PearsonCorrelation.class;

    public final static Class HYBRID_VECTOR_NORMALIZER = MeanVarianceNormalizer.class;

    // how many modules similar to a module marked as notInterested should be decreased
    public final static int HYBRID_SIMILAR_MODULES_COUNT = 10;

    // by how many stars a module, similar to a module that has been marked as notInterested, should be decreased
    public final static double HYBRID_SIMILAR_MODULE_DECREASE = 0.3;


    // CB Recommender
    public final static Class CB_VECTOR_SIMILARITY = PearsonCorrelation.class;

    public final static Class CB_VECTOR_NORMALIZER = MeanVarianceNormalizer.class;

    // minimum average rating for a module to get an increase
    public final static double CB_MIN_AVG_RATING = 4;

    // by how many stars a module with an average rating above CB_MIN_AVG_RATING should be increased
    public final static double CB_PREDICTION_BOOST_MULTIPLIER = 2;


    // CF Recommender

    public final static Class CF_VECTOR_NORMALIZER = MeanVarianceNormalizer.class;

    public final static Class CF_VECTOR_SIMILARITY = PearsonCorrelation.class;

    public final static int CF_NEIGHBORHOOD_SIZE = 10;

    public final static int CF_MIN_NEIGHBOR_COUNT = 1;
}
